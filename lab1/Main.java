import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;

class MemoryTest {
    public int size;
    public int time;
    public MemoryTest() {
    }

    public MemoryTest size(int size) { 
        this.size = size;
        return this;
    }

    public MemoryTest time(int time) {
        this.time = time;
        return this;
    }
    
    public void test() {
        int arr[] = new int[size * 1024 * 1024];
        try {
            Thread.sleep(time);
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
}

class CpuTest {
    public int size;

    public CpuTest size(int size) {
        this.size = size;
        return this;
    }

    public void test() {
        Random rand = new Random();
        int arr[] = new int[size];
        for(int i = 0 ; i < size ; i++) arr[i] = rand.nextInt();
        Arrays.sort(arr);
    }
}

public class Main {

    public static void main(String args[]) throws Exception {
        if(args.length == 0) {
            System.out.println("Usage TYPE=<CPU/MEM> KEY1=VAL1 KEY2=VAL2");
            System.exit(1);
        }
        System.out.println("[   ok  ] process id: " + ProcessHandle.current().pid());
        Files.write(Paths.get("process.pid"), (ProcessHandle.current().pid()+"").getBytes(), StandardOpenOption.CREATE);
        String TYPE = args[0].split("=")[1];
        switch (TYPE) {
            case "CPU" -> {
                new CpuTest().size(Integer.parseInt(args[1].split("=")[1])).test();
            }
            case "MEM" -> {
                new MemoryTest()
                    .size(Integer.parseInt(args[1].split("=")[1]))
                    .time(Integer.parseInt(args[2].split("=")[1]))
                    .test();
            }
            default -> {
                System.out.printf("Not a valid test type %s, Available test types: CPU / MEM \n", TYPE);
            }
        }
        Thread.sleep(2000);
    }
}
