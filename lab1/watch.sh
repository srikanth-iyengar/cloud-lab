#!/bin/sh

process_pid=$(cat process.pid)

watch -n 1 "ps aux --pid=$process_pid --sort=-%mem | awk '{size=\$6/1024; size_gb=size/1024; printf \"%s\t%.2f GB\t%s\n\", \$2, size_gb, \$11}' | head -n 11"

