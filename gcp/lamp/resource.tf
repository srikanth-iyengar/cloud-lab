provider "google" {
  credentials = file("../credentials.json") # Replace with your GCP service account key
  project     = "good-project-124"          # Replace with your GCP project ID
  region      = "us-central1"               # Replace with your desired region
}

variable "gcloud_username" {
  type = string
}

resource "google_compute_instance" "lamp_instance" {
  name         = "lamp-instance"
  machine_type = "e2-medium"
  zone         = "us-central1-a"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11" # Replace with your desired OS image
    }
  }

  network_interface {
    network = "default"
    access_config {}
  }

  metadata = {
    ssh-keys = "ksrikanth1777:${file("./creds/gcloud.pub")}"
  }

  tags = ["web"]

  metadata_startup_script = <<-EOF
    #!/bin/bash
    sudo apt-get update -y
    sudo apt-get install apache2 php -y
    sudo systemctl enable apache2
    sudo systemctl start apache2
    EOF
}

resource "google_compute_firewall" "default" {
  name    = "test-firewall"
  network = "default"

  allow {
    protocol = "tcp"
    ports    = ["80", "8080", "3306"]
  }

  source_ranges = ["0.0.0.0/0"]
}

resource "google_sql_database_instance" "lamp_db_instance" {
  name                = "lamp-db-instance"
  database_version    = "MYSQL_5_7"
  project             = "good-project-124"
  region              = "us-central1"
  deletion_protection = false

  settings {
    tier = "db-f1-micro"
    ip_configuration {
      dynamic "authorized_networks" {
        for_each = google_compute_instance.lamp_instance
        iterator = apps

        content {
          name  = google_compute_instance.lamp_instance.name
          value = google_compute_instance.lamp_instance.network_interface[0].access_config[0].nat_ip
        }
      }
    }
  }
}

resource "google_sql_user" "lamp_db_user" {
  name     = "lamp-db-user"
  password = "your-db-password"
  instance = google_sql_database_instance.lamp_db_instance.name
  host     = "%"
}

resource "google_sql_database" "lamp_db" {
  name     = "lamp_db"
  instance = google_sql_database_instance.lamp_db_instance.name
}

output "lamp_instance_ip" {
  value = google_compute_instance.lamp_instance.network_interface[0].access_config[0].nat_ip
}

output "lamp_db_internal_ip" {
  value = google_sql_database_instance.lamp_db_instance.private_ip_address
}

output "lamp_db_instance" {
  value = google_sql_database_instance.lamp_db_instance.name
}

output "lamp_db_username" {
  value = google_sql_user.lamp_db_user.name
}
