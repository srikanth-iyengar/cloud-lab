#!/bin/bash

echo "Retrieving for the google sql for the database ✅"
INSTANCE_NAME=$(terraform output -json | jq '.lamp_db_instance .value' -r)
USERNAME=$(terraform output -json | jq '.lamp_db_username .value' -r)

echo "Metadata retrieval succesfull ✅"
echo "Trying to connect to google sql "
gcloud sql connect "$INSTANCE_NAME" --user="$USERNAME" < init_db.sql
