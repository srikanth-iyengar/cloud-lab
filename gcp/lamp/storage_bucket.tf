resource "google_storage_bucket" "static-site" {
  name          = "srikanth1777_yashshingade28"
  location      = "EU"
  force_destroy = true
  project       = "good-project-124"

  uniform_bucket_level_access = true

  website {
    main_page_suffix = "index.html"
    not_found_page   = "404.html"
  }
  cors {
    origin          = ["*"]
    method          = ["GET", "HEAD", "PUT", "POST", "DELETE"]
    response_header = ["*"]
    max_age_seconds = 3600
  }
}
