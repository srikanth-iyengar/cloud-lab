#!/bin/bash

sudo cp /tmp/index.php /var/www/html
echo "Changed the php file to the pushed one ✅"
cd /var/www/html
sudo rm -f index.html
sudo systemctl restart apache2
echo "Restarted the Apache Webserver ✅"
sudo systemctl status apache2
