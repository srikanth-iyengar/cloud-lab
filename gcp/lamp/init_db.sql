USE lamp_db;
CREATE TABLE IF NOT EXISTS users (
    id INT AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    age INT,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

DELIMITER //
CREATE PROCEDURE InsertRandomUsers()
BEGIN
  DECLARE i INT DEFAULT 0;
  
  WHILE i < 30 DO
    INSERT INTO users (username, email, age)
    VALUES (
      CONCAT('user', i),
      CONCAT('user', i, '@example.com'),
      FLOOR(RAND() * 60 + 18)
    );
    
    SET i = i + 1;
  END WHILE;
END //
DELIMITER ;

CALL InsertRandomUsers();

DROP PROCEDURE IF EXISTS InsertRandomUsers;
