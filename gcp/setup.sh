#!/bin/zsh

curl -O https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-cli-446.0.1-linux-x86_64.tar.gz
tar -xZVF ./google-cloud-cli-446.0.1-linux-x86_64.tar.gz
cd ./google-cloud-sdk
./install.sh
source ~/.zshrc
gcloud auth login
gcloud projects create good-project-124 --name="Cloud Computing"
gcloud config set project good-project-124
gcloud iam service-accounts create my-service-account --display-name "My Service Account"
gcloud iam service-accounts keys create credentials.json --iam-account my-service-account@good-project-124.iam.gserviceaccount.com
gcloud services enable sqladmin.googleapis.com
gcloud services enable compute.googleapis.com
mkdir -p lamp/creds
ssh-keygen -t rsa -f lamp/creds/gcloud -N ""
